# Research regarding acceptance testing in the cloud

## Questions

 - Cloud integration testing
 - Testing in production
 - Cloud acceptance testing
 - ATDD, BDD, Specification by Example
 - Integration tests in Spring
 - Integration tests in Angular
 - PACT 
 - HowTo: acceptance tests ins Spring
 - Examples: Spring and Cucumber
 - Examples: Spring and Spock
 - Search "BDD in Spring"
 - Search "ATDD in Spring"
 - Search "Specification By Example in Spring"

## Sources

 - Wikipedia  
 - generic search engines
 - Medium
 - Conference talks
 - Online learning portals
 - Youtube
 - Baeldung

## Research

### Cloud integration testing

### TLDR
According to IEEE, integration testing is the testing of functional requirements in a system or subsystem.
A list of types of integration testing can be found in the "Path" section.
Here are a list of integration testing philosophies I stumbeled upon while reserarching:
 - Philosophy 1: Run integration tests in pipeline. The standard approach if it fits the given environment. A bottom-up approach to integration testing might be suited best here.
 - Philosophy 2: Test functional requirements in production. There is a seperate section in this document on this.
 - Philosophy 3: Test everything with multiple suites of testing tools. This is "What Netflix does", because that seems to be very popular to state.
 - Philosophy 4: Let a seperate runner run user defined tests all the time. If one of these tests breaks, devs are informed.

### Path
Definition of integration testing: https://en.wikipedia.org/wiki/Integration_testing, with a source from IEEE 24765:2010(E)

Types of integration testing: https://www.guru99.com/integration-testing.html

Philosophy 2: https://thenewstack.io/how-to-do-microservices-integration-testing-in-the-cloud/

Philosophy 3: https://digitalscot.net/cloud-native-software-testing-transformation/

Philosophy 4: https://medium.com/@gajus/automating-persistent-integration-testing-and-alerting-d507572a2618

### Testing in production

### TLDR
Since it is -by the estimation of many qa engineers- almost impossible to keep the qa environment an exact copy of the production environment,
with all its constraints and parameters, the idea of Testing in production emerged.
This is mostly a qa persons territory, so here just a quick overview.
 - Philosophy 1: Do it manually at times with low load and monitor like crazy. Also keep your test and real data and credentials seperate.
 - Philosophy 2: The "deploy" stage of a project is split into "deploy", "release" and "post release" and a number of testing and monitoring techniques are used in each to find bugs.

### Path
Philosohpy 1: https://www.neotys.com/blog/tips-for-testing-in-production/

Philosophy 2: https://medium.com/@copyconstruct/testing-in-production-the-safe-way-18ca102d0ef1

### Cloud acceptance testing

### TLDR
This research will follow the XP definition of Acceptance Testing. Acceptance tests arise from the acceptance criteria given by a user story. 
By doing this the functional requirements of the story are tested. They are run as black-box tests and automated to the highest practicable degree.
The story is not "done" until the tests succeed and these can be run at any time to serve as functional regression tests.

Popular frameworks to write Acceptance Tests are sometimes called "BDD-", "ATDD", or "Specification by Example" frameworks. 
Though not all of these are exactly the same they achive similar things: A test is written in a non-technical language and filled with example data 
from the acceptance criteria. See later chapters in this document.

There is another definition of Acceptance Testing that puts the acceptance tests above system tests. These have a much broader scope. 
This kind of Acceptance Testing is not in the focus of this research. 

### Path
Definition of Acceptance Tests: https://en.wikipedia.org/wiki/Acceptance_testing

Definition of Acceptance Tests in XP: http://www.extremeprogramming.org/rules/functionaltests.html

Non-XP Acceptance Tests explained further: http://softwaretestingfundamentals.com/acceptance-testing

Extensive Overview of Acceptance Tests (german): https://blog.seibert-media.net/blog/2011/05/18/akzeptanztests-scrum-agile-softwareentwicklung/

Example for writing Acceptance Tests: https://dzone.com/articles/untangling-concepts-unit-tests

Acceptance Tests from a customer point-of-view: https://www.informit.com/articles/article.aspx?p=167788

### BDD, SBE, ATDD

### TLDR
According to their Wiki pages, Behaviour Driven Development (BDD), Specification by Example (SBE) and ATDD (Acceptance Test Driven Development) are three aspects,
albeit on different levels, of the same idea. The idea is to think in the wanted behaviour of some chunk of code first, then formulating some expectation that can be used
as a test (preferrably automatic) to prove that the source code behaves in the formulated way.

In this, BDD borrows from DDD to find expected behaviours and business value. There are multiple phases of analysis and implementation, like with DDD.
Behaviour tests are written for technical implementations and functional and non-functional requirements. One such test describes an expected behaviour,
ranging from "should throw an exception if ..." to "... updates my account balance when I deposit XX units of currency ...". 

SBE deals with expectations that come from the business side and encapsulates the idea that part of a requirement is formulated as examples on how to meet that requriement.
This is generally done as a user story, with the examples as acceptance criteria. Acceptance Tests are derived from these examples to test if requirements are fullfilled.
One best practice is to have a single source of truth, which means creating one document of examples. Tests and Documentation are automatically generated from the examples.

ATDD is the activity of writing Acceptance Tests first and then using these to check requirements and use them as regression tests to make
sure new changes do not break finished functionality. The original idea of the three amigos (Dev, Biz, QA) working together on the Acceptance Test suite stems from ATDD.

### Path
BDD:
https://en.wikipedia.org/wiki/Behavior-driven_development
https://behaviourdriven.org/


SBE:
https://en.wikipedia.org/wiki/Specification_by_example
https://www.thoughtworks.com/insights/blog/specification-example
https://www.martinfowler.com/bliki/SpecificationByExample.html

ATDD:
https://en.wikipedia.org/wiki/Acceptance_test%E2%80%93driven_development

### Integration Testing in Spring

### TLDR
The distributed nature of cloud native applications lends itself to a more bottom-up approach to integration testing. Integration testing over the confines 
of one microservice, deployed ui, or similar structure can make either the integration test environment expensive or the test very brittle. 
In the next chapters we will have a look at the methods to to integration tests for your distributed modules (servies, ui, ...)
and how to make sure that they play nicely together without testing them all at once.

Integration testing in spring is done by using different kinds of testing annotations that will give varying degrees over things like:
 - Mocked/Real live dependencies
 - Data Source handling

The most common Integration Tests are:
 - SpingBootTest with RestTestTemplate: This kind of test creates a complete Spring Environment and your tests are black-box tests that fire requests and receive responses.
 - WebMvcTest or WebFluxTest: This kind of tests lets you test your controller, either Non-Reactive or Reactive. Dependencies can either be mocked for Unit level testing or autowired for integration testing.

Spring Boot also allows you to use a fake database at development time that is connected via configuration and will automatically swapped for the real thing in QA or Production.

### Path
SpringBootTest with RestTestTemplate: https://howtodoinjava.com/spring-boot2/testing/spring-integration-testing/

Levels of Integration Tests: https://dzone.com/articles/unit-and-integration-tests-in-spring-boot

Testing & Integration Testing at spring.io:

https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/testing.html

https://docs.spring.io/autorepo/docs/spring-framework/4.2.0.RC2/spring-framework-reference/html/integration-testing.html

### Integration Tests in Angular

### TLDR
Like Spring, Angular provides an environment that allows you to perform tests of varying width and depth.
 - The TestBed API allows initializing components and services within a test environment, and also to use mocks where needed
 - The Protractor framework enables developers to write tests that check the actual DOM. 
 
 These are the tools needed to create integration tests for frontends. 

### Path
Unit and integration tests in Angular: https://dev.to/kayis/isolated-unit-testing-and-deep-integration-testing-in-angular-19lg

Unit and integration test (Three parts): https://medium.com/@ana.dvorkina/unit-and-integration-tests-for-angular-components-323a2c681972

Angular Testbed: https://angular.io/api/core/testing/TestBed

Protractor Framework: https://www.protractortest.org/#/

### PACT

### TLDR

As was alluded to in the "Spring" chapter, a bottom up approach to Integration testing (meaning we pick and choose the modules we integrate and test) 
can leave interfaces between modules untested. In cloud native development these modules could be a bunch of microservices, frond-ends or any other
sytem those depend on. If the choice of the above chapters to test services and frontends by themselves is made then it stands to reason we need another
testing strategy to make sure they play together nicely. One tool to reach that goal is PACT.
In Pact, an interface contract is defined and tooling is provided to check if all players in a system are still fullfilling this contract.
The tooling is talking to a central report server that can show the current state of the contracts, consumers (systems that need data), and 
providers (systems that provide data).

### Path

PACT: https://docs.pact.io/
